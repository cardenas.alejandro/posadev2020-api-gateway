package net.acardenas.posadev2020.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApiGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApiGatewayApplication.class, args);
  }

  @Bean
  public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
    return builder.routes()
      .route("ordersRoute", r -> r.path("/order/**")
        .filters(f -> f.stripPrefix(1))
        .uri("lb://order-service"))
      .route("inventoryRoute", r -> r.path("inventory/**")
        .filters(f -> f.stripPrefix(1))
        .uri("lb://inventory-service"))
      .build();
  }

}
