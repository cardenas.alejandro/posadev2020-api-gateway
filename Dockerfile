FROM adoptopenjdk/openjdk11:alpine-jre
RUN apk add tzdata
ADD target/api-gateway.jar api-gateway.jar
RUN mkdir -p /applogs
VOLUME ["/applogs"]
CMD [ "java", "-jar", "/api-gateway.jar" ]