# POSADEV 2020 - API Gateway

El proposito de esta aplicacion es redireccionar los request al servicio pertinente, para este demo:

-   `/order`  -> Sera enviado al `order-service`
-   `/inventory` -> Sera enviado al `inventory-service`

Para iniciarlo enviando los logs en formato JSON a la consola:

```shell
docker-compose -f docker-compose-local.yml up 
```
Para iniciarlo creando logs planos y en formato json:

```shell
docker-compose up 
```

Puedes agregar `-d` al final para ejecutarlo en modo detached

Es importante cambiar el volume para que apunte a un directorio real en la maquina host:

```yaml
    volumes:
      - ~/posadev2020logs:/applogs:rw
```